package com.btsinfo.appliintro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText eTFace;
    private Button  bTLancer;
    private Spinner spFace;
    private Spinner spDes;
    private String choixface;
    private String choixdes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bTLancer = (Button) findViewById(R.id.bTLancer);
        bTLancer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toExecut();
            }
        });
        spFace=(Spinner)findViewById(R.id.spFace);
        spDes=(Spinner)findViewById(R.id.spDes);

        final ArrayAdapter<CharSequence>adapter=ArrayAdapter.createFromResource(this,R.array.facedispo,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFace.setAdapter(adapter);

        spFace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixface=adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(),choixface,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<CharSequence>adapter2=ArrayAdapter.createFromResource(this,R.array.nbdes,android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDes.setAdapter(adapter2);

        spDes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                choixdes=adapterView.getItemAtPosition(i).toString();
                Toast.makeText(adapterView.getContext(),choixdes,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void toExecut(){
        Intent intent=new Intent(this,ActivityResult.class);
        intent.putExtra( "Face",choixface);
        intent.putExtra( "Des",choixdes);
        startActivity(intent);
    }
}
