package com.btsinfo.appliintro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.security.SecureRandom;

public class ActivityResult extends AppCompatActivity {

    TextView tVtest;
    TextView tVtest2;
    TextView tVtest3;
    TextView tVtest4;
    TextView tVtest5;
    Button btRelance;
    SecureRandom random=new SecureRandom();
    int face2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tVtest = (TextView) findViewById(R.id.tVtest);
        tVtest2=(TextView) findViewById(R.id.tVtest2);
        tVtest3=(TextView) findViewById(R.id.tVtest3);
        tVtest4=(TextView) findViewById(R.id.tVtest4);
        tVtest5=(TextView) findViewById(R.id.tVtest5);

        Intent newintent= getIntent();

        String face=newintent.getStringExtra("Face");
        face2=Integer.parseInt(face);

        String des=newintent.getStringExtra("Des");
        int des2=Integer.parseInt(des);

        btRelance = (Button) findViewById(R.id.btRelance);
        btRelance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jet(face2);
            }
        });

        jet(face2);

        if (des2 >1)
        {
            tVtest2.setVisibility(View.VISIBLE);
            if (des2>2)
            {
                tVtest3.setVisibility(View.VISIBLE);
                if (des2>3)
                {
                    tVtest4.setVisibility(View.VISIBLE);
                    if (des2>4)
                    {
                        tVtest5.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }
    private void jet(int face2)
    {
        int test=random.nextInt(face2);
        test+=1;
        int test2=random.nextInt(face2);
        test2+=1;
        int test3=random.nextInt(face2);
        test3+=1;
        int test4=random.nextInt(face2);
        test4+=1;
        int test5=random.nextInt(face2);
        test5+=1;

        String testS1= Integer.toString(test);
        tVtest.setText(testS1);

        String testS2= Integer.toString(test2);
        tVtest2.setText(testS2);

        String testS3= Integer.toString(test3);
        tVtest3.setText(testS3);

        String testS4= Integer.toString(test4);
        tVtest4.setText(testS4);

        godmode(test5,face2);

        String testS5= Integer.toString(test5);
        tVtest5.setText(testS5);

    }
    private void godmode(int r,int face2)
    {
        if (face2==20) {
            while (r < 15) {
                r = random.nextInt(face2);
            }
        }
    }

}
